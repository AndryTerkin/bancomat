package com.gmail.zc.xtyzz;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

public class Bank {
    private String bankName;                                        //Банк имеет имя
    private HashMap<Card, Integer> cardAccounts = new HashMap();    //Коллекцию карт-щетов
    private int numberOfaccounts;                                   //Колличество карт-щетов
    private Long timer = 60000L;                                    //Банк устанавливает срок на который выдает клиенту карту

    public Bank(String bankName) {
        this.bankName = bankName;
        this.numberOfaccounts = 0;
    }

    public String getBankName() {
        return bankName;
    }

    public Integer showCash(Card card) {//Банк по запросу показывает сколько денег на карте клиента
        return cardAccounts.get(card);
    }

    public boolean isAnoughMoney(Card card, int value) {//Метод проверяет возможен ли вывод данной суммы
        return (cardAccounts.get(card) >= value);
    }

    public void withdrawMoney(Card card, Integer value) {//При выводе средств банк уменьшает коллчество денег на щету
        int newValue = cardAccounts.get(card) - value;
        cardAccounts.remove(card);
        cardAccounts.put(card, newValue);
    }

    public Card createNewCard(String clientName) {         //Банк выдает карту конкретному клиенту
        Long currentData = (new Date()).getTime();          //с конретным сроком пользавания
        ++numberOfaccounts;                                 //номер сщета
        int pinCode = (new Random()).nextInt(10000);  //и случайным пин-кодом
        Card card = new Card(clientName, pinCode, numberOfaccounts, currentData + timer, this);
        this.cardAccounts.put(card, 500);                   //при создании на карту зачисляется 500 единиц
        return card;
    }

    public Bankomat createNewBankomat(Bank bank) {
        return new Bankomat(this);
    }   //Банк создаем банкомат


    public class Card {             //Класс карты
        private String clientName;  //На ней имя клиента
        private int pinCode;        //Пин-код клиенту
        private int cardAccount;    //Карт щет
        private Long validity;      //Срок действия
        private Bank bank;          //Логотип банка, который выдал карту

        public Card(String clientName, int pinCode, int cardAccount, Long validity, Bank bank) {
            this.clientName = clientName;
            this.pinCode = pinCode;
            this.cardAccount = cardAccount;
            this.validity = validity;
            this.bank = bank;
            System.out.println("Новая карта создана, не потеряйте пин-код " + pinCode);
        }

        public String getClientName() {
            return clientName;
        }

        public Long getValidity() {
            return validity;
        }

        public boolean security(int pinCode) {
            return (this.pinCode == pinCode);
        }
    }

    public class Bankomat {         // Банкомат знает какой банк его создал и колличество средств в нем
        private Bank bank;
        private int cupon20;
        private int cupon10;
        private int cupon5;
        private int sum;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        public Bankomat(Bank bank) {
            this.bank = bank;
            cupon5 = 20;
            cupon10 = 20;
            cupon20 = 20;
        }

        private boolean isValid(Card card) {
            return new Date().getTime() >= card.getValidity();
        }

        private boolean isMultiplied(int sum) {
            int c5 = sum / 5;
            if (sum - c5 * 5 != 0 ||                                                             //Число не кратно 5
                    (c5 % 2 != 0 && cupon5 == 0) ||                                               //Число кратно 5 но в банкомате нет пятерок
                    (((c5 > 3) || c5 % 4 != 0) && (cupon10 == 0 && cupon5 < 2)) ||                //Число меньше или не кратно 20 и в банкомате не наберется 10
                    (c5 == 3 && ((cupon5 < 1 && cupon10 < 1) || (cupon10 == 0 && cupon5 < 3)))    //Нужно вывести 15 но 15 не набирается
                    ) return false;
            else return true;
        }

        private boolean checkPinCode(Card card) {
            try {
                System.out.println("Введите пин код");
                int pin = Integer.parseInt(reader.readLine());
                return (card.security(pin));    //пин код проверяется в карточке, банкомат про него ничего не знает
            } catch (Exception e) {             // а также инструменты для вывода средств
                System.out.println("Ошибка ввода");
                return false;
            }

        }

        private void wait10() {             //пяти секундная задержка
            try {
                System.out.println();
                for (int i = 0; i < 5; i++) {
                    System.out.print(".");
                    Thread.sleep(1000);

                }
                System.out.println();
            } catch (InterruptedException e) {
            }
        }

        private boolean readBoolean() {     //Чтение булевого выражения
            try {
                boolean result;
                System.out.println("true или другой символ");
                result = Boolean.parseBoolean(reader.readLine());
                return result;
            } catch (Exception e) {
                System.out.println("Ошибка ввода");
                return false;
            }

        }

        private int readInt() {         //Чтение целого числа
            try {
                int result = Integer.parseInt(reader.readLine());
                return result;
                } catch (Exception e) {
                System.out.println("Ошибка ввода");
                return 0;
            }
        }

        private int moneyInBancomat() {
            return cupon10 * 10 + cupon20 * 20 + cupon5 * 5;
        }

        private void giveMoney(int sum, Card card) { //Вывод средств
            bank.withdrawMoney(card, sum);
            int c20 = 0;            // Банкомат выдает сумму по возможности большими номиналами
            int c10 = 0;
            int c5 = 0;
            c20 = sum / 20;
            if (cupon20 < c20) c20 = cupon20;
            sum -= c20 * 20;
            c10 = sum / 10;
            if (cupon10 < c10) c10 = cupon10;
            sum -= c10 * 10;
            c5 = sum / 5;

            System.out.println("Банкомат выдаст " + c20 + " двадцаток " + c10 + " десяток " + c5 + " пятерок");
            cupon20 -= c20;
            cupon10 -= c10;
            cupon5 -= c5;


            try {
                System.out.println("трррр");
                Thread.sleep(1000);
                System.out.println("пшшшш");
                Thread.sleep(1000);
                System.out.println("Заберите ваши деньги");
            } catch (InterruptedException e) {
            }
        }

        private boolean waitForMoney() {   //Добавление купюр в банкомат если пользователь согласен ждать

            System.out.println("Желаете подождать пока персонал устранит проблему?");
            if (readBoolean()) {
                wait10();
                cupon5 += 20;
                cupon10 += 20;
                cupon20 += 20;
                return true;
            } else return false;
        }

        private void goodBue() {                // Завершение работы с пользователем
            System.out.println("Заберите карту");
            System.out.println("Досвидания");

        }

        public void useIt(Card card) {          // Основной метод общения с пользователем
            System.out.println("Здравствуйте " + card.getClientName() + " спасибо что воспользавались банкоматом " + bank.getBankName() + " банка");
            while (true) {
                if (isValid(card)) { //Проверка срока действия карточки
                    System.out.println("К сожалению срок действия вашей карты истек, обратитесь в банк");
                    break;
                }
                if (!checkPinCode(card)) { //Далее проверка пин-кода
                    System.out.println("Неправильный код");
                    System.out.println("Следующая попытка будет доступна через 10 секунд");
                    wait10();
                    continue;
                }
                System.out.println("Сейчас на вашем счету " + bank.showCash(card));
                System.out.println("Желаете снять наличные?");
                if (!readBoolean()) {
                    goodBue();
                    break;
                }
                System.out.println("В банкомате есть купюры номиналов ");//Если все в порядке банкомат предлагает выполнить вывод средств
                if (cupon20 != 0) System.out.print("20 ");
                if (cupon10 != 0) System.out.print("10 ");
                if (cupon5 != 0) System.out.print("5 ");
                System.out.println("рублей");

                System.out.println("Введите сумму которую хотите обналичить");
                sum = readInt();
                if (!bank.isAnoughMoney(card, sum)){                     //Банкомат проверяет возможность вывода введеной суммы
                    System.out.println("На вашем счету недостаточно средств");
                    if (!waitForMoney()) {
                        goodBue();
                        break;
                    }
                }
                else if (sum > moneyInBancomat()) {
                    System.out.println("В банкомате недостаточно денег");
                    if (!waitForMoney()) {
                        goodBue();
                        break;
                    }
                    else {
                        System.out.println("Проблема устранена");
                        continue;
                    }
                } else if (!isMultiplied(sum)) {
                    System.out.println("Вывод невозможен так как нет необходимых номиналов купюр или введена некратная номиналам сумма");
                    if (!waitForMoney()) {
                        goodBue();
                        break;
                    }
                    else {
                        System.out.println("Проблема устранена");
                        continue;
                    }
                } else giveMoney(sum, card);
                System.out.println("Желаете продолжить? (true or another token)");
                if (!readBoolean()) {
                    goodBue();
                    break;
                }
            }
        }
    }
}






