package com.gmail.zc.xtyzz;



public class Main {

    public static void main(String[] args) {
        Person client = new Person("BORIS IVANOV");     //Создаем пользователя
        Bank bps = new Bank("BPS");                 //Создаем банк
        Bank.Card myFirstcard = bps.createNewCard(client.getName());    //Клиент заказывает карту в банке
        Bank.Bankomat bpsBancomat = bps.createNewBankomat(bps);         //Банк создает свой банкомат
        bpsBancomat.useIt(myFirstcard); //Клиент Пользуется своей первой картой
    }
}
